package Model;

/**
 * Created by Nassim on 21/06/2017.
 */
public class User {

    private String firstName;
    private String lastName;
    private String email;
    private String emailConfirmation;
    private String passwd;
    private Birthdate birthdate;
    private String sex;

    public User(String firstName, String lastName, String email, String emailConfirmation, String passwd, Birthdate birthdate, String sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.emailConfirmation = emailConfirmation;
        this.passwd = passwd;
        this.birthdate = birthdate;
        this.sex = sex;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailConfirmation() {
        return emailConfirmation;
    }

    public String getPasswd() {
        return passwd;
    }

    public Birthdate getBirthdate() {
        return birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmailConfirmation(String emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setBirthdate(Birthdate birthdate) {
        this.birthdate = birthdate;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}