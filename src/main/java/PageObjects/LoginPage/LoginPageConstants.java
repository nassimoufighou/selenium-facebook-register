package PageObjects.LoginPage;

/**
 * Created by Nassim on 20/06/2017.
 */
public class LoginPageConstants {

    public static final String pageHeader_xpath = ".//*[@id='content']/div/div/div/div/div[2]/div[1]/div[1]";
    public static final String firstName_name = "firstname";
    public static final String lastname_name = "lastname";
    public static final String email_name = "reg_email__";
    public static final String email_name_confirm = "reg_email_confirmation__";
    public static final String passwd_name = "reg_passwd__";
    public static final String submit_btn_name = "websubmit";
    public static final String error_icon_xpath = ".//*[@id='u_0_9']/i";
}
