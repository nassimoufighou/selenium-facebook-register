package PageObjects.LoginPage;

import Utils.Utils;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

/**
 * Created by Nassim on 19/06/2017.
 */
public class LoginPage {

    private WebDriver webDriver;
    private ExtentTest test;
    @FindBy(xpath = LoginPageConstants.pageHeader_xpath) private WebElement pageHeader;
    @FindBy(name = LoginPageConstants.firstName_name) private WebElement firstName_name;
    @FindBy(name = LoginPageConstants.lastname_name) private WebElement lastName_name;
    @FindBy(name = LoginPageConstants.email_name) private WebElement email_name;
    @FindBy(name = LoginPageConstants.email_name_confirm) private WebElement email_name_confirmation;
    @FindBy(name = LoginPageConstants.passwd_name) private WebElement passwd_name;
    @FindBy(name = LoginPageConstants.submit_btn_name) private WebElement submit_btn;
    @FindBy(xpath = LoginPageConstants.error_icon_xpath) private WebElement error_icon;

    public LoginPage(WebDriver webDriver, ExtentTest test) {
        this.webDriver = webDriver;
        this.test = test;
        PageFactory.initElements(webDriver, this);
    }

    public void isPageLoaded(){
        boolean isDisplayed = this.pageHeader.isDisplayed();
        test.log(LogStatus.INFO, "Browser is up, redirecting to <b>" + webDriver.getCurrentUrl() + "</b>");
        if (isDisplayed) test.log(LogStatus.PASS,"Login page is loaded" + test.addScreenCapture(Utils.captureScreenshot(webDriver, "login_page")));
        else{
            test.log(LogStatus.FAIL, "Header is not present");
            Assert.assertFalse(isDisplayed);
        }
    }

    public void fillFirstName(String firstname){
        this.firstName_name.sendKeys(firstname);
        test.log(LogStatus.PASS, "'Firstname': <b>" + firstname + "</b");
    }

    public void fillLastName(String lastName){
        this.lastName_name.sendKeys(lastName);
        test.log(LogStatus.PASS, "'Lastname': <b>" + lastName + "</b>");
    }

    public void fillEmail(String email){
        this.email_name.sendKeys(email);
        test.log(LogStatus.PASS, "'Email': <b>" + email + "</b>");
    }

    public void fillEmailConfirmation(String email){
        this.email_name_confirmation.sendKeys(email);
        test.log(LogStatus.PASS, "'Email confirmation': <b>" + email + "</b>");
    }

    public void fillPasswd(String passwd){
        this.passwd_name.sendKeys(passwd);
        test.log(LogStatus.PASS, "'Password': <b>" + passwd + "</b>");
    }

    public void selectSex(String sex){
        if (sex.equals("Female")){
            webDriver.findElement(By.id("u_0_h")).click();
            test.log(LogStatus.PASS, "<b> 'Female' </b> option selected");
        }
        else if (sex.equals("Male")){
            webDriver.findElement(By.id("u_0_i")).click();
            test.log(LogStatus.PASS, "<b> 'Male' </b> option selected");
        }
    }

    public void selectBirthdayDay(int day) {
        webDriver.findElement(By.xpath(".//select[@id=\"day\"]/option[@value=" + String.valueOf(day) + "]")).click();;
        test.log(LogStatus.PASS, "<b>" + day + " </b> birthday day selected");
   }

    public void selectBirthdayMonth(String month) {
        int monthInt = 0;
        for (Map.Entry<String, Integer> entry : Utils.months.entrySet()) if(entry.getKey().equals(month)) monthInt = entry.getValue();
        webDriver.findElement(By.xpath(".//select[@id=\"month\"]/option[@value=" + monthInt + "]")).click();
        test.log(LogStatus.PASS, "<b>" + month + " </b> birthday month selected");
   }

    public void selectBirthdayYear(int year) {
        webDriver.findElement(By.xpath(".//select[@id=\"year\"]/option[@value=" + String.valueOf(year) + "]")).click();
        test.log(LogStatus.PASS, "<b>" + year + " </b> birthday year selected");
   }

   public void submit(){
       submit_btn.click();
       test.log(LogStatus.PASS, "<b> 'Submit' </b> button clicked");
   }

   public void errorOnPage(){
       if(error_icon.isDisplayed()) test.log(LogStatus.FAIL, "There is an error on the registration form " + test.addScreenCapture(Utils.captureScreenshot(webDriver, "error")));
       Assert.assertFalse(error_icon.isDisplayed());
   }
}
