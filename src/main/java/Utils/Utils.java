package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Nassim on 20/06/2017.
 */
public class Utils {

    public static HashMap<String, Integer> months = new HashMap<String, Integer>() {{
        put("Enero", 1);
        put("Febrero", 2);
        put("Marzo", 3);
        put("Abril", 4);
        put("Mayo", 5);
        put("Junio", 6);
        put("Julio", 7);
        put("Agosto", 8);
        put("Septiembre", 9);
        put("Octubre", 10);
        put("Noviembre", 11);
        put("Diciembre", 12);

    }};

    public static Properties readPropertiesFile(){
        Properties properties = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(".//src//main//resources//config.properties");
            properties.load(input);
        }
        catch (IOException ex) { ex.printStackTrace(); }
        finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return properties;
    }

    public static String captureScreenshot(WebDriver driver, String name) {
            String dest = null;
            try{
                TakesScreenshot ts = (TakesScreenshot)driver;
                File source = ts.getScreenshotAs(OutputType.FILE);
                dest = Utils.readPropertiesFile().getProperty("SCREENSHOT_PATH") + name + ".png";
                System.out.println(dest);
                File destination = new File(dest);
                FileUtils.copyFile(source, destination);
            }
            catch (IOException ioe) {}
            return dest;
    }
}