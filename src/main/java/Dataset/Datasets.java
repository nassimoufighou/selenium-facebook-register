package Dataset;

import Model.Birthdate;
import Model.User;
import org.testng.annotations.DataProvider;

/**
 * Created by Nassim on 21/06/2017.
 */
public class Datasets {

    @DataProvider
    public static Object[][] facebookLoginDP1(){
        User user = new User(   "Mike",
                                "Johnson",
                                "qa2017.auto1@gmail.com",
                                "qa2017.auto1@gmail.com",
                                 "1234567890ñsfhñdbfaifsl",
                                 new Birthdate(07, "Agosto", 1942),
                                "Male");
        return new Object[][]{{user}};
    }

    @DataProvider
    public static Object[][] facebookLoginDP2(){
        User user = new User(   "N",
                                "McNish",
                                "qa2017.auto1@gmail.com.",
                                "qa2017.auto1@gmail.com",
                                 "1234567890ñsfhñdbfaifsl",
                                 new Birthdate(13, "Mayo", 1965),
                                "Female");
        return new Object[][]{{user}};
    }
}
