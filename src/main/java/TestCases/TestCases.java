package TestCases;

import Dataset.Datasets;
import Model.User;
import Utils.Utils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.*;
import PageObjects.LoginPage.LoginPage;

import java.io.File;
import java.util.Properties;

/**
 * Created by Nassim on 19/06/2017.
 * All test cases are implemented in this class. The report result is stored in the project root folder as "report.html"
 */
public class TestCases {

    private WebDriver webDriver;
    private ExtentReports report;
    private ExtentTest test;
    private Properties properties;

    @BeforeSuite
    public void beforeSuite(){
        properties = Utils.readPropertiesFile();
        report = new ExtentReports(properties.getProperty("REPORT_PATH"), true);
        report.loadConfig(new File(properties.getProperty("REPORT_CONFIG")));
    }

    @BeforeMethod
    public void setupBrowser(){
        properties = Utils.readPropertiesFile();
        ChromeDriverManager.getInstance().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        webDriver = new ChromeDriver(options);
        webDriver.get(properties.getProperty("URL"));
    }

    @Test(dataProvider = "facebookLoginDP1", dataProviderClass = Datasets.class, description = "Dataset for a new Facebook user")
    public void facebookLogin(User user) throws InterruptedException {
        try {
            test = report.startTest("Facebook new user", "Register a new Facebook profile");
            test.assignAuthor("Nassim Oufighou");
            LoginPage loginPage = new LoginPage(webDriver, test);
            loginPage.isPageLoaded();
            loginPage.fillFirstName(user.getFirstName());
            Thread.sleep(1000);
            loginPage.fillLastName(user.getLastName());
            Thread.sleep(1000);
            loginPage.fillEmail(user.getEmail());
            Thread.sleep(1000);
            loginPage.fillEmailConfirmation(user.getEmailConfirmation());
            Thread.sleep(1000);
            loginPage.fillPasswd(user.getPasswd());
            Thread.sleep(1000);
            loginPage.selectBirthdayDay(user.getBirthdate().getDay());
            Thread.sleep(1000);
            loginPage.selectBirthdayMonth(user.getBirthdate().getMonth());
            Thread.sleep(1000);
            loginPage.selectBirthdayYear(user.getBirthdate().getYear());
            Thread.sleep(1000);
            loginPage.selectSex(user.getSex());
            Thread.sleep(1000);
            loginPage.submit();
            Thread.sleep(1000);
        }
        catch (InterruptedException ie){}

        finally {
            report.endTest(test);
        }
    }

    @Test(dataProvider = "facebookLoginDP2", dataProviderClass = Datasets.class, description = "Dataset for a new Facebook user")
    public void facebookWrongLogin(User user) throws InterruptedException {
        try {
            test = report.startTest("Facebook new wrong user", "Register a wrong new Facebook profile");
            test.assignAuthor("Nassim Oufighou");
            LoginPage loginPage = new LoginPage(webDriver, test);
            loginPage.isPageLoaded();
            loginPage.fillFirstName(user.getFirstName());
            Thread.sleep(1000);
            loginPage.fillLastName(user.getLastName());
            Thread.sleep(1000);
            loginPage.fillEmail(user.getEmail());
            Thread.sleep(1000);
            loginPage.fillEmailConfirmation(user.getEmailConfirmation());
            Thread.sleep(1000);
            loginPage.fillPasswd(user.getPasswd());
            Thread.sleep(1000);
            loginPage.selectSex(user.getSex());
            Thread.sleep(1000);
            loginPage.selectBirthdayDay(user.getBirthdate().getDay());
            Thread.sleep(1000);
            loginPage.selectBirthdayMonth(user.getBirthdate().getMonth());
            Thread.sleep(1000);
            loginPage.selectBirthdayYear(user.getBirthdate().getYear());
            Thread.sleep(1000);
            loginPage.submit();
            Thread.sleep(1000);
            loginPage.errorOnPage();
            Thread.sleep(1000);
        }
        catch (InterruptedException ie){}

        finally {
            report.endTest(test);
        }
    }

    @AfterMethod
    public void tear(ITestResult iTestResult) {
        report.flush();
        int status = iTestResult.getStatus();
        switch (status){
            case 1:
                test.log(LogStatus.PASS, test.getTest().getName() + " TEST PASSED! ");
                break;
            case 2:
                test.log(LogStatus.FAIL, iTestResult.getThrowable());
                break;
            case 3:
                test.log(LogStatus.SKIP, test.getTest().getName() + " TEST SKIPPED");
                break;
        }
        webDriver.close();
    }

    @AfterSuite
    public void afterSuite(){
        report.close();
    }
}

